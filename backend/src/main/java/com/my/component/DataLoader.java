package com.my.component;

import com.my.entity.Role;
import com.my.entity.Users;
import com.my.repository.RoleRepository;
import com.my.repository.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;

import static com.my.entity.enums.RoleName.*;

@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.datasource.initialization-mode}")
    private String initialMode;

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;

    public DataLoader(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) {
        if (initialMode.equals("always")) {
            roleRepository.save(new Role(1, ROLE_OWNER));
            roleRepository.save(new Role(2, ROLE_USER));
            roleRepository.save(new Role(3, ROLE_ADMIN));
            userRepository.save(new Users(
                    "Sherzod",
                    "G'ulomqodirov",
                    "sherzod_off",
                    passwordEncoder.encode("S7edge"),
                    roleRepository.findAllByNameIn(
                            Collections.singletonList(ROLE_OWNER))));
        }
    }
}
