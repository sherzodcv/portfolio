package com.my.entity;

import com.my.entity.abstractClass.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PhoneVerify extends AbsEntity {

    private String phoneNumber;
    private Integer verifyCode;
    private boolean isVerify;

}
