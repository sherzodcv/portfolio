package com.my.entity;

import com.my.entity.abstractClass.AbsEntity;
import com.my.entity.enums.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Portfolios extends AbsEntity {


    private String gitLabLink;
    private String instagramLink;
    private String youTubeLink;
    private String title;

    @Enumerated(EnumType.STRING)
    private Category category;

    @ManyToOne
    private Attachment attachment;
}
