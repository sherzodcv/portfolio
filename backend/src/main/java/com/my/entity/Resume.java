package com.my.entity;

import com.my.entity.abstractClass.AbsEntity;
import com.my.entity.enums.Proglang;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Resume extends AbsEntity {

    @Enumerated(EnumType.STRING)
    private Proglang proglang;
}
