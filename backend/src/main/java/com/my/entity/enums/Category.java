package com.my.entity.enums;

public enum Category {

    ANIMATION,
    BACKEND,
    FRONTEND,
    WEBFULLSTACK
}
