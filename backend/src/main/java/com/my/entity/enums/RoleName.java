package com.my.entity.enums;

public enum RoleName {

    ROLE_OWNER,
    ROLE_ADMIN,
    ROLE_USER

}
