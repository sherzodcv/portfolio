package com.my.entity.enums;

public enum Region {

    TASHKENT,
    ANDIJAN,
    BUKXARA,
    FERGANA,
    NAMANGAN,
    NAVOIY,
    JIZZAX,
    SAMARQAND,
    QASHQADARYO,
    SURXONDARYO,
    SIRDARYO,
    XORAZM,
    QORAQALPOGISTON
}
