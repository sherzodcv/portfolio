package com.my.entity.enums;

public enum Proglang {
    HTML,
    CSS,
    JAVA_SCRIPT,
    REACTJS,
    VUEJS,
    ANGULARJS,
    UMIJS,
    JAVA,
    POSTGRESQL,
    SPRING,
    SPRINGBOOT,
    HIBERNATE,
    TOMCAT,
    PHP,
    LARAVEL,
    DJANGO,
    CSHARP,
    C,
    PYTHON,
    DART,
    FLUTTER,
    NODEJS
}
