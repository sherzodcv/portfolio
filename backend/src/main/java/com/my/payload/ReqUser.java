package com.my.payload;

import com.my.entity.enums.Region;
import lombok.Data;

import java.time.LocalDate;
import java.util.UUID;

@Data
public class ReqUser {

    private UUID id;

    private String firstName;

    private String lastName;

    private String password;

    private String phoneNumber;

    private String username;

    private Region region;

    private LocalDate birthDate;

    private String newPassword;

    private UUID attachmentId;
}

