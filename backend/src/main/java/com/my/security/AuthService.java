package com.my.security;

import com.my.controller.AuthController;
import com.my.entity.Users;
import com.my.entity.enums.RoleName;
import com.my.exceptions.ResourceNotFound;
import com.my.payload.ApiResponse;
import com.my.payload.ReqUser;
import com.my.repository.AttachmentRepository;
import com.my.repository.RoleRepository;
import com.my.repository.UserRepository;
import com.my.service.TwilioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.UUID;

@Service
public class AuthService implements UserDetailsService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final AttachmentRepository attachmentRepository;

    @Autowired
    TwilioService twilioService;

    @Autowired
    AuthController authController;

    public AuthService(@Lazy UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository, AttachmentRepository attachmentRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.attachmentRepository = attachmentRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }

    UserDetails loadUserById(UUID userId) {
        return userRepository.findById(userId).orElseThrow(() -> new UsernameNotFoundException("User id not found: " + userId));
    }

    public ApiResponse saveOrEditUser(ReqUser reqUser) {
        if (reqUser.getId() != null) {
            try {
                Users user = userRepository.findById(reqUser.getId()).orElseThrow(() -> new ResourceNotFound("User", "id", reqUser.getId()));
                user.setFirstName(reqUser.getFirstName());
                user.setLastName(reqUser.getLastName());
                user.setUsername(reqUser.getUsername());
                user.setPhoneNumber(reqUser.getPhoneNumber());
                user.setPassword(passwordEncoder.encode(reqUser.getPassword()));
                user.setRegion(reqUser.getRegion());
                user.setBirthDate(reqUser.getBirthDate());
                if (reqUser.getAttachmentId() != null) {
                    user.setAttachment(attachmentRepository.findById(reqUser.getAttachmentId()).orElseThrow(() -> new ResourceNotFound("Attachment", "id", reqUser.getAttachmentId())));
                }
                userRepository.save(user);
                return new ApiResponse("Successfully edited 😊😊😊", true, user);
            } catch (ResourceNotFound resourceNotFound) {
                return new ApiResponse("Didn't edited 😥😥😥", false);
            }
        } else {
            try {
                Users user = new Users();
                user.setFirstName(reqUser.getFirstName());
                user.setLastName(reqUser.getLastName());
                user.setUsername(reqUser.getUsername());
                user.setPassword(passwordEncoder.encode(reqUser.getPassword()));
                user.setPhoneNumber(reqUser.getPhoneNumber());
                user.setRoles(roleRepository.findAllByNameIn(
                        Collections.singletonList(RoleName.ROLE_USER)));
                userRepository.save(user);
                return new ApiResponse("Successfully saved 😊😊😊", true, user);
            } catch (Exception e) {
                return new ApiResponse("Didn't saved 😥😥😥", false);
            }
        }
    }

    public ApiResponse deleteUser(UUID userId) {
        try {
            userRepository.deleteById(userId);
            return new ApiResponse("success", true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error", false);
    }

    public ApiResponse changeEnabled(UUID userId) {
        return new ApiResponse("User " + (userRepository.changeEnabledById(userId) ? "enabled" : "disabled"), true);
    }

    public ApiResponse changePassword(ReqUser reqUser) {

        try {
            Users user = userRepository.findById(reqUser.getId()).orElseThrow(() -> new ResourceNotFound("User", "id", reqUser.getId()));
            if (user.getPassword().equals(reqUser.getPassword())) {
                user.setPassword(reqUser.getNewPassword());
                userRepository.save(user);
                return new ApiResponse("success", true);
            }
        } catch (Exception e) {
            return new ApiResponse("Password don't matches !!!", false);
        }
        return new ApiResponse("User not found !!!", false);
    }
}