package com.my.repository;


import com.my.entity.Role;
import com.my.entity.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RoleRepository extends JpaRepository<Role, Integer> {

    List<Role> findAllByNameIn(List<RoleName> name);

    //    Optional<Role> findByName(RoleName name);
}
