package com.my.repository;


import com.my.entity.PhoneVerify;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PhoneVerifyRepository extends JpaRepository<PhoneVerify, Integer> {

    Optional<PhoneVerify> findByPhoneNumber(String phoneNumber);
}
