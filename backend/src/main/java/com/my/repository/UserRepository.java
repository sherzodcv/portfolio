package com.my.repository;

import com.my.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<Users, UUID> {
    Optional<Users> findByUsername(String username);

    @Transactional
    @Query(value = "update users set enabled = not enabled where id=:dtId returning enabled",
            nativeQuery = true)
    boolean changeEnabledById(@Param(value = "dtId") UUID id);

}