package com.my.repository;

import com.my.entity.Portfolios;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PortfolioRepository extends JpaRepository<Portfolios, UUID> {

}
