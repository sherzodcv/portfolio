package com.my.model;

import com.my.entity.enums.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class ReqPortfolio {

    private String gitLabLink;
    private String instagramLink;
    private String youTubeLink;
    private String title;
    private Category category;
    private UUID attachmentId;
}