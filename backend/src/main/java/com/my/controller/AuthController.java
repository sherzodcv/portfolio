package com.my.controller;

import com.my.entity.Users;
import com.my.payload.ApiResponse;
import com.my.payload.JwtResponse;
import com.my.payload.ReqSignIn;
import com.my.payload.ReqUser;
import com.my.repository.UserRepository;
import com.my.security.AuthService;
import com.my.security.CurrentUser;
import com.my.security.JwtTokenProvider;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private final JwtTokenProvider jwtTokenProvider;

    private final AuthenticationManager authenticate;

    private final com.my.security.AuthService AuthService;

    private final UserRepository userRepository;

    public AuthController(JwtTokenProvider jwtTokenProvider, AuthenticationManager authenticate, AuthService AuthService, UserRepository userRepository) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticate = authenticate;
        this.AuthService = AuthService;
        this.userRepository = userRepository;
    }

    @PostMapping("/login")
    public HttpEntity<?> login(@RequestBody ReqSignIn reqSignIn) {
        return ResponseEntity.ok(getApiToken(reqSignIn.getUsername(), reqSignIn.getPassword()));
    }

    private HttpEntity<?> getApiToken(String username, String password) {
        Authentication authentication = authenticate.authenticate(
                new UsernamePasswordAuthenticationToken(username, password)
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        String jwt = jwtTokenProvider.generateToken(authentication);
        return ResponseEntity.ok(new JwtResponse(jwt));
    }

    @GetMapping
    public HttpEntity<?> getAll() {
        List<Users> all = userRepository.findAll();
        return ResponseEntity.ok(all);
    }

    @GetMapping("/search")
    public HttpEntity<?> search(@RequestParam String search) {
        Optional<Users> username = userRepository.findByUsername(search);
        if (username.isPresent()){
            return ResponseEntity.status(200).body(1);
        }else {
            return ResponseEntity.status(200).body(null);
        }
    }

    @GetMapping("/me")
    public HttpEntity<?> userMe(@CurrentUser Users user) {
        return ResponseEntity.status(user != null ? 200 : 409).body(user);
    }

    @PostMapping("/saveUser")
    public HttpEntity<?> saveUser(@RequestBody ReqUser reqUser) {
        ApiResponse apiResponse = AuthService.saveOrEditUser(reqUser);
        if (apiResponse.isSuccess()) {
            return getApiToken(reqUser.getUsername(), reqUser.getPassword());
        }
        return ResponseEntity.status(409).body(apiResponse);
    }

    @PutMapping("/editUser")
    public HttpEntity<?> editUser(@RequestBody ReqUser reqUser) {
        ApiResponse apiResponse = AuthService.saveOrEditUser(reqUser);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @DeleteMapping("/deleteUser")
    public HttpEntity<?> deleteUser(@RequestParam UUID userId) {
        ApiResponse apiResponse = AuthService.deleteUser(userId);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PutMapping("/changeEnabled")
    public HttpEntity<?> changeEnabled(@RequestParam UUID userId) {
        ApiResponse apiResponse = AuthService.changeEnabled(userId);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }

    @PostMapping("/change/password")
    public HttpEntity<?> changePassword(@RequestBody ReqUser reqUser) {
        ApiResponse apiResponse = AuthService.changePassword(reqUser);
        return ResponseEntity.status(apiResponse.isSuccess() ? 200 : 409).body(apiResponse);
    }
}