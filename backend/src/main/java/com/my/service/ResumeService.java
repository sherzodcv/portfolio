package com.my.service;

import com.my.repository.AttachmentRepository;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.pdmodel.encryption.StandardProtectionPolicy;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

@Service
public class ResumeService {

    @Autowired
    AttachmentRepository attachmentRepository;

    public void createPdf(HttpServletResponse response) throws IOException {

        PDDocument document = new PDDocument();
        PDPage myPage = new PDPage();
        document.addPage(myPage);
//        String imageUrl = "F:\\Projects\\Personal Project\\portfolio\\backend\\src\\main\\resources\\avatar.jpg";
        String imageUrl = "F:\\Projects\\Personal Project\\portfolio\\backend\\src\\main\\resources\\avatar.jpg";
        PDImageXObject pdImage = PDImageXObject.createFromFile(imageUrl, document);

        AccessPermission accessPermission = new AccessPermission();

        StandardProtectionPolicy spp = new StandardProtectionPolicy("1237", "1237", accessPermission);
        spp.setEncryptionKeyLength(128);
        spp.setPermissions(accessPermission);
        document.protect(spp);

        try (PDPageContentStream contentStream = new PDPageContentStream(document, myPage)) {
            contentStream.drawImage(pdImage, 1, 620, 170, 170);
        }
        OutputStream outputStream = response.getOutputStream();
        document.save(outputStream);
    }
//    public static void main(String[] args) {
//        File urlFile = new File("F:\\Projects\\Personal Project\\portfolio\\backend\\src\\main\\resources\\Resume.pdf");
//
//        try (PDDocument document = PDDocument.load(urlFile)) {
//            PDPage myPage = new PDPage();
//            document.addPage(myPage);
//            String imageUrl = "F:\\Projects\\Personal Project\\portfolio\\backend\\src\\main\\resources\\avatar.jpg";
//            PDImageXObject pdImage = PDImageXObject.createFromFile(imageUrl, document);
//
//            int width = pdImage.getWidth();
//            int height = pdImage.getHeight();
//
//            float offset = 20f;
//
//            try (PDPageContentStream contentStream = new PDPageContentStream(document, myPage)) {
//                contentStream.drawImage(pdImage, offset, offset, width, height);
//            }
//            document.save("F:\\portfolio.pdf");
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
}