package com.my.service;


import com.my.entity.PhoneVerify;
import com.my.payload.ApiResponse;
import com.my.payload.ReqSms;
import com.my.repository.PhoneVerifyRepository;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Random;

@Service
public class TwilioService {

    private final PhoneVerifyRepository phoneVerifyRepository;

    @Value("AC1b177b7a0476430679a1ae974e0361f3")
    private String accountSid;

    @Value("132f96c3d71cfe5331d97b0bdadfa226")
    private String authToken;

    @Value("+16626232827")
    private String trialPhoneNumber;

    public TwilioService(PhoneVerifyRepository phoneVerifyRepository) {
        this.phoneVerifyRepository = phoneVerifyRepository;
    }

    public ApiResponse sendSms(String phoneNumber) {
        try {
            String checkedPhoneNumber = (phoneNumber.startsWith("+") ? phoneNumber.replace(" ", "") : "+" + phoneNumber.replace(" ", ""));
            Twilio.init(accountSid, authToken);
            int code = generateCode();
            Message message = Message.creator(new PhoneNumber(checkedPhoneNumber), new PhoneNumber(trialPhoneNumber), "Your verification code: " + code).create();
            Optional<PhoneVerify> byPhoneNumber = phoneVerifyRepository.findByPhoneNumber(phoneNumber);
            if (byPhoneNumber.isPresent()) {
                PhoneVerify phoneVerify = byPhoneNumber.get();
                phoneVerify.setVerifyCode(code);
                phoneVerify.setVerify(false);
                phoneVerifyRepository.save(phoneVerify);
            } else {
                PhoneVerify phoneVerify = new PhoneVerify();
                phoneVerify.setPhoneNumber(phoneNumber);
                phoneVerify.setVerifyCode(code);
                phoneVerify.setVerify(false);
                phoneVerifyRepository.save(phoneVerify);
            }
            return new ApiResponse("successfully", true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ApiResponse("error", false);
    }

    public int generateCode() {
        return new Random().nextInt(900000) + 100000;
    }

    public ApiResponse checkSms(ReqSms reqSms) {
        Optional<PhoneVerify> byPhoneNumber = phoneVerifyRepository.findByPhoneNumber(reqSms.getPhoneNumber());
        if (byPhoneNumber.isPresent()) {
            PhoneVerify phoneVerify = byPhoneNumber.get();
            if (phoneVerify.getVerifyCode() == reqSms.getVerifyCode()) {
                phoneVerify.setVerify(true);
                phoneVerifyRepository.save(phoneVerify);
                return new ApiResponse("success", true);
            } else {
                return new ApiResponse("error", false);
            }
        } else {
            return new ApiResponse("ERROR", false);
        }
    }
}
