import React from 'react';
import Tittle from "../components/Tittle";
import ImageSection from "./ImageSection";
import SkillsSection from "../components/SkillsSection";
import ServiceSection from "../components/ServiceSection";
import react from '../images/react.png';
import web from '../images/responsive.png';
import java from '../images/java.png';

function AboutPage() {
    return (
        <div className="aboutPage">
            <Tittle tittle={'About Me'} span={'About Me'}/>
            <ImageSection/>
            <Tittle tittle={'My Skills'} span={'My Skills'}/>
            <div className="skillContainer">
                <SkillsSection skill={'Javascript'} progress={'30%'} width={'30%'}/>
                <SkillsSection skill={'React Js'} progress={'55%'} width={'55%'}/>
                <SkillsSection skill={'HTML'} progress={'85%'} width={'85%'}/>
                <SkillsSection skill={'CSS'} progress={'85%'} width={'85%'}/>
                <SkillsSection skill={'SASS'} progress={'70%'} width={'70%'}/>
                <SkillsSection skill={'Web Design'} progress={'60%'} width={'60%'}/>
                <SkillsSection skill={'Java Core'} progress={'75%'} width={'75%'}/>
                <SkillsSection skill={'Java OOP'} progress={'75%'} width={'75%'}/>
                <SkillsSection skill={'PostgreSQL'} progress={'65%'} width={'65%'}/>
                <SkillsSection skill={'Spring Boot'} progress={'60%'} width={'60%'}/>
            </div>

            <Tittle tittle={'Services'} span={'Services'}/>
            <div className="services-container">
                <ServiceSection image={web} title={'Design'} text={'Creating responsive design'}/>
                <ServiceSection image={react} title={'React JS'} text={'Create web app in react js'}/>
                <ServiceSection image={java} title={'Java'} text={'Create application in java.'}/>
            </div>
        </div>
    );
}

export default AboutPage;