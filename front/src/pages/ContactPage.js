import React from 'react';
import ContactItem from "../components/ContactItem";
import phone from '../images/phone.png';
import gmail from '../images/gmail.png';
import location from '../images/location.png';
import Tittle from "../components/Tittle";

function ContactPage() {
    return (
        <>
            <div className="b-title">
                <Tittle tittle={'Contact'} span={'Contact'}/>
            </div>
            <div className="ContactPage">
                <div className="map-sect">
                    <iframe title="iframe"
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d192431.40494199801!2d68.90761400666611!3d41.09651176507058!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38ae7a58ed0e4c63%3A0xbe1a74821f285e2e!2sYangiyul%20District%2C%20Uzbekistan!5e0!3m2!1sen!2s!4v1624351039588!5m2!1sen!2s"
                        width="600" height="450" style={{border:0}} allowFullScreen="" loading="lazy"/>
                </div>
                <div className="contact-sect">
                    <ContactItem icon={phone} text1={'+998(99)-464-66-36'} title={'Phone'}/>
                    <ContactItem icon={gmail} text1={'sherzodofff@gmail.com'}
                                 title={'Gmail'}/>
                    <ContactItem icon={location} text1={'Yangiyul, Tashkent Region'} title={'Location'}/>
                </div>
            </div>
        </>
    );
}

export default ContactPage;