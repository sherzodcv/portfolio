import React from 'react';

function HomePage() {
    return (
        <div className="HomePage">
            <h1 className="hero-text">
                Hi, I am
                <span> Sherzod</span>
            </h1>
            <p className="h-sub-text">
                My name is Sherzod, my surname is G'ulomqodirov. I was born 15<sup>th</sup> august in 2001. When I was 7
                years old, I went to the 1<sup>st</sup> grade of
                secondary school № 52 in Yangiyul district of Tashkent region.
                I graduated from the 9<sup>th</sup> grade of this school in 2017 and continued my studies in the
                10<sup>th</sup> grade of this
                school. After graduating from the 11<sup>th</sup> grade of this school in 2019, I entered the Personal
                Development
                Process IT academy in Tashkent in October 2019 and graduated from this academy in December 2020. I am
                currently a 2nd-grade student of Information Systems and Technologies at the National University of
                Uzbekistan.
            </p>
            <div className="icons">
                <a href="https://www.facebook.com/Sherzod.cv">
                    <img className="icon-holder fb"
                         src="https://img.icons8.com/color/48/000000/facebook-circled--v3.png" alt="😁😁"/>
                </a>
                <a href="https://github.com/sherzodcv">
                    <img className="icon-holder github" src="https://img.icons8.com/color/48/000000/github.png"
                         alt="😁"/>
                </a>
                <a href="https://gitlab.com/sherzod_nnn">
                    <img className="icon-holder gitlab" src="https://img.icons8.com/color/48/000000/gitlab.png"
                         alt="😁"/>
                </a>
                <a href="https://instagram.com/sherzod.cv">
                    <img className="icon-holder ins" src="https://img.icons8.com/fluent/48/000000/instagram-new.png"
                         alt="😁"/>
                </a>
            </div>
        </div>
    );
}

export default HomePage;