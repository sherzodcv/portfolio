import React from 'react';
import about from '../images/me.jpg'
import myResume from '../resume/G\'ulomqodirov Sherzod.pdf';

function ImageSection() {
    return (
        <div className="ImageSection">
            <div className="img">
                <img src={about} alt=""/>
            </div>
            <div className="about-info">
                <h4>I am <span> Sherzod</span></h4>
                <p className="about-text">
                    I live in Tashkent region Yangiyul district.
                    In the following lines I have given a brief account of myself: 👇🏻👇🏻👇🏻👇🏻👇🏻👇🏻
                </p>
                <div className="about-details">
                    <div className="left-section">
                        <p>Full Name</p>
                        <p>Age</p>
                        <p>Nationality</p>
                        <p>Languages</p>
                        <p>Address</p>
                        <p>Country</p>
                    </div>
                    <div className="right-section">
                        <p>: Sherzod G'ulomqodirov</p>
                        <p>: 20</p>
                        <p>: Uzbek</p>
                        <p>: Uzbek, Russian, English</p>
                        <p>: Tashkent region</p>
                        <p>: Uzbekistan</p>
                    </div>
                </div>
                <a href={myResume} download={"Sherzod's resume.pdf"} className="downloadBtn">
                    Download Resume
                </a>
            </div>
        </div>
    );
}

export default ImageSection;